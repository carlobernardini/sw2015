import re
import json
from pprint import pprint

def word_in_text (needle, haystack):
	needle = needle.lower()
	haystack = haystack.lower()
	match = re.search(needle, haystack)
	if match:
		return True
	else:
		return False

def count_occurrences (mapping, tweets_store, matched_output = False):
	if matched_output != False:
		output_file = open (matched_output, 'w')

	tweet_str = open (tweets_store, 'r')
	tweets = json.load(tweet_str)

	parties_file = open (mapping, 'r')
	parties_str = parties_file.read()
	parties = json.loads(parties_str)

	occurrences = {k: { 'count': 0, 'positive': 0, 'negative': 0, 'unknown': 0 } for k, v in parties.items()}

	matches = 0
	total = 0

	for tweet in tweets:
		tweet_has_match = False
		total += 1

		for party in parties:
			party_has_match = False

			for synonym in parties[party]:
				if word_in_text(synonym, tweet['text']) and party_has_match == False:
					tweet_has_match = True
					party_has_match = True
					matches += 1
					occurrences[party]['count'] +=1

		if tweet_has_match == True and matched_output != False:
			json.dump(tweet, output_file)
			output_file.write('\n')

	print '>>> %d tweets were processed' % total
	print '>>> %d synonyms / hashtags were matched' % matches

	pprint(occurrences)

	outfile = open ('combined_matched_occurrences.json', 'w')
	json.dump(occurrences, outfile, indent=4)

count_occurrences ('parties_mapped.json', 'datasamples/combined_matched_id_nomultiple-3.json')