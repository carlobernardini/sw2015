<!DOCTYPE html>
<html lang="en">
<head>
    <title>Social Web 2015 App - Group 7</title>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta property="og:url" content="http://sw2015.bernardini.nl/">
    <meta property="og:image" content="http://sw2015.bernardini.nl/img/og_image.jpg">
    <meta property="og:title" content="2015 Provincial Elections BuzzCharts">
    <meta property="og:description" content="A crowd-sourced analysis of the 2015 Dutch Provincial Elections. Multiple visualizations representing mention counts, frequently used terms and sentiments towards individual political parties, based on Twitter data.">
    <link rel="icon" type="image/png" sizes="192x192"  href="img/favicon/android-icon-192x192.png">
    <link rel="icon" type="image/png" sizes="32x32" href="img/favicon/favicon-32x32.png">
    <link rel="icon" type="image/png" sizes="96x96" href="img/favicon/favicon-96x96.png">
    <link rel="icon" type="image/png" sizes="16x16" href="img/favicon/favicon-16x16.png">
    <link rel="manifest" href="img/favicon/manifest.json">
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400italic,400,600,700,300' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Roboto:500,900,400italic,300,700,400' rel='stylesheet' type='text/css'>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/css/bootstrap.min.css">
    <link rel="stylesheet" href="//maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/c3.min.css" type="text/css">
    <link rel="stylesheet" href="css/app.css" type="text/css" media="all">
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body data-spy="scroll" data-target="#main-nav">
    <div id="fb-root"></div>
    <script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.0";
        fjs.parentNode.insertBefore(js, fjs);
        }(document, 'script', 'facebook-jssdk'));
    </script>
	<nav class="navbar navbar-inverse navbar-fixed-top" id="main-nav">
		<div class="container">
	   		<div class="navbar-header">
      			<a class="navbar-brand" href="index.php">
      				<img src="img/vulogo_2.jpg" height="20" style="display:inline-block;" />
      				Social Web 2015 Group 7
      			</a>
    		</div><!-- .navbar-header -->
            <div class="collapse navbar-collapse">
                <ul class="nav navbar-nav">
                    <li><a href="#chart-container"><i class="fa fa-twitter"></i> Mention Count</a></li>
                    <li><a href="#donut-container"><i class="fa fa-heartbeat"></i> Sentiment Analysis</a></li>
                    <li><a href="#timeline-container"><i class="fa fa-comments"></i> Debate Timeline</a></li>
                    <li><a href="#concept-cloud"><i class="fa fa-cloud"></i> Term Cloud</a></li>
                </ul>
            </div><!-- .collapse -->
		</div>
	</nav>
    <div class="container">
        <div class="page-header">
            <h1>
                Provincial Elections BuzzCharts<br />
                <small>An automated analysis of crowd sourced data about the Dutch Provincial Elections of 2015</small>
                <div class="hidden" id="loading">&nbsp;</div>
            </h1>
        </div>
        <div class="row">
            <div class="col-sm-3 col-xs-6">
                <address>
                    <strong><a href="https://nl.linkedin.com/in/carlobernardini" target="_blank">Carlo Bernardini</a></strong> <i class="fa fa-external-link"></i> (Development)<br>
                    Faculty of Sciences, VU<br>
                    Amsterdam, NL<br>
                    <abbr title="E-mail">E:</abbr> 1934481@student.vu.nl
                </address>
            </div>
            <div class="col-sm-3 col-xs-6">
                <address>
                    <strong>H&uuml;sn&uuml; Ta&#351;</strong> (Statistics)<br>
                    Faculty of Sciences, UvA<br>
                    Amsterdam, NL<br>
                    <abbr title="E-mail">E:</abbr> 2525616@student.vu.nl
                </address>
            </div>
            <div class="col-sm-3 col-xs-6">
                <address>
                    <strong>Deepak Soekhoe</strong> (Mining)<br>
                    Faculty of Sciences, VU<br>
                    Amsterdam, NL<br>
                    <abbr title="E-mail">E:</abbr> 1997424@student.vu.nl
                </address>
            </div>
            <div class="col-sm-3 col-xs-6">
                &nbsp;
            </div>
        </div>
        <div class="well well-sm">
            <i class="fa fa-info-circle"></i> The current data set holds <span id="num-tweets" style="font-weight:bold;">0</span> tweets that matched <span id="num-tracks">0</span> tracked parties
        </div>
    </div>
    <div class="container section" id="chart-container">
        <h2><i class="fa fa-twitter"></i> Mention Count</h2>
        <p class="text-muted">
            This visualization displays how often individual political parties were mentioned within the
            collected data sample of tweets. This visualization <strong>does not</strong> take into 
            consideration sentiment classifications.
        </p>
        <div class="row">
            <div class="col-sm-12 clearfix" id="bar-chart">

            </div>
        </div>
    </div><!-- #chart-container -->
    <div class="container section" id="donut-container">
        <h2>
            <i class="fa fa-heartbeat"></i> Sentiment Analysis
            <div class="attribution">
                API provided by <a href="http://ai-applied.nl/sentiment-analysis-api/" target="_blank" rel="nofollow">Ai Applied</a> <img src="img/ai-applied.png" alt="API provided by Ai Applied" style="display:inline-block" height="30" />
            </div>
        </h2>
        <p class="text-muted">
            Which sentiment do people express towards individual parties on twitter? These donut charts visualize the 
            distribution of positive and negative sentiments towards individual parties on the basis of collected tweet texts.
            For this analysis we used a sentiment analysis API from Ai Applied, who kindly allowed us access to execute an 
            increased amount of API calls.<br />
            Tweets mentioning multiple parties were filtered out of the analyzed set since the judgement concerns the entire 
            tweet text. The confidence threshold was set to >=0.75 &mdash; meaning tweets with a confidence grade below 0.75 are ignored &mdash; to enhance the reliability of the judgements.
        </p>
    </div><!-- #donut-container -->
    <div class="container section" id="timeline-container">
        <h2><i class="fa fa-comments"></i> Final Debate Timeline</h2>
        <p class="text-muted">
            For this visualization we mined tweets during the final electoral debate on Tuesday March 
            18<sup>th</sup>. We collected a set of 6717 tweets when tracking <kbd>#PS15</kbd>, the official 
            hashtag for the debate <a href="http://nos.nl/artikel/2025200-verkiezingsdebat-op-npo-1.html" target="_blank" rel="nofollow">according to the NOS <i class="fa fa-external-link"></i></a>.
            From these tweets, we selected the ones mentioning one or more hashtags that we assigned to each party beforehand. Then, we split up the tweets in
            timeframes of 5 minutes from the beginning (20:30) until the end (21:55) of the debate.
        </p>
        <div class="col-xs-12 col-md-9 clearfix" id="timeseries">
            &nbsp;
        </div>
        <div class="col-xs-12 col-md-3 clearfix" id="pie-chart">
            &nbsp;
        </div>
    </div>
    <div class="container">
        <div class="well well-sm">
            <i class="fa fa-fast-forward"></i> Click on a node in the timeline above to seek to the corresponding moment in the video below.
        </div>
        <div class="col-xs-12 embed-responsive embed-responsive-16by9 clearfix">
            <div id="player" class="embed-responsive-item"></div>
        </div>
    </div><!-- #timeline-container-->
    <div class="container section" id="concept-cloud" style="position:relative;">
        <h2>
            <i class="fa fa-cloud"></i> Term Cloud
            <div class="attribution">
                API provided by <a href="http://ai-applied.nl/text-label-api/" target="_blank" rel="nofollow">Ai Applied</a> <img src="img/ai-applied.png" alt="API provided by Ai Applied" style="display:inline-block" height="30" />
            </div>
        </h2>
        <div class="col-xs-12 clearfix">
            <p class="text-muted">
                In addition, we also performed a text analysis of all tweets, extracting the main concepts for each tweet and summarizing them according
                to their frequency and impact. Each concept label was assigned a certain weight, which is taken into account in the summary. Again, an API 
                by Ai Applied was used for this analysis.
            </p>
            <div class="btn-group">
                <button type="button" class="btn btn-default full-list" data-toggle="modal" data-target="#textlabel-list">
                    <i class="fa fa-bars"></i> List of terms
                </button>
                <button type="button" class="btn btn-default hidden terms-action reset-inline" data-action="shuffle">
                    <i class="fa fa-undo"></i>
                </button>
                <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-expanded="false" id="filter-terms-dropdown">
                    <span class="caret"></span>
                    <span class="sr-only">Toggle Dropdown</span>
                </button>
                <ul class="dropdown-menu dropdown-menu-right" role="menu">
                    <li><a href="#" class="terms-action" data-action="filter"><i class="fa fa-times"></i> Remove terms containing (party) names</a></li>
                    <li><a href="#" class="terms-action" data-action="shuffle"><i class="fa fa-undo"></i> Reset cloud</a></li>
                </ul>
            </div>
        </div>
        <div class="col-xs-12 clearfix" id="word-cloud">

        </div>
    </div>
    <footer class="footer">
        <div class="container" style="position:relative;">
            <p class="text-muted">
                &copy; 2015 - <strong>Group 7</strong> <a href="http://thesocialweb2015.wordpress.com" rel="nofollow" target="_blank">Social Web</a> Elective - Vrije Universiteit Amsterdam
            </p>
            <div class="social">
                <a href="https://twitter.com/share" class="twitter-share-button" data-url="http://sw2015.bernardini.nl/" data-hashtags="sw2015">Tweet</a>
                <div class="fb-like" data-href="http://sw2015.bernardini.nl/" data-layout="button_count" data-action="like" data-show-faces="true" data-share="true"></div>
                <script type="IN/Share" data-url="http://sw2015.bernardini.nl/" data-counter="right"></script>
            </div>
        </div>
    </footer>
    <div class="modal fade" id="textlabel-list">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title"><i class="fa fa-cloud"></i> Top 100 terms and respective rounded weights</h4>
                </div>
                <div class="modal-body" style="max-height:500px; overflow:auto; overflow-x:hidden;">
                    <ul class="list-group"></ul>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-primary" data-dismiss="modal"><i class="fa fa-times"></i> Close list</button>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog --> 
    </div><!-- /.modal -->
    <script src="http://cdnjs.cloudflare.com/ajax/libs/headjs/1.0.3/head.min.js"></script>
    <script>!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0],p=/^http:/.test(d.location)?'http':'https';if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src=p+'://platform.twitter.com/widgets.js';fjs.parentNode.insertBefore(js,fjs);}}(document, 'script', 'twitter-wjs');</script>
    <script src="//platform.linkedin.com/in.js" type="text/javascript"> lang: en_US</script>
    <script>
        head.load(
            'https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js',
            'https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js',
            'http://d3js.org/d3.v3.min.js',
            'js/c3.min.js',
            'js/d3.layout.cloud.js',
            'https://www.youtube.com/iframe_api',
            'js/app.js'
        );
    </script>
</body>
</html>
