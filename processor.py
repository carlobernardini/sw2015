import re
import requests
import json
from pprint import pprint

#### global configuration parameters ####

api_key_file = open ('ai_applied_key.json', 'r')
api_key_dict = json.load(api_key_file)
ai_applied_apikey = api_key_dict['ai_applied_apikey']
ai_applied_endpoint = 'http://api.ai-applied.nl/api/sentiment_api/' # sentiment analysis API endpoint
tracked_words = ['#ps2015', 'vvd', 'pvda', 'cda', 'pvv', 'sp', 'd66', 'groen links'] # list of words to track
tweets_store = 'datasamples/_datasample.json' # file containing mined data sample
analysis_output_file = 'analyzed.json' # file to write analysis output to
occurrences_output_file = 'occurrences.json'
tweets_data = []
num_tweets_to_process = 50 # number of tweets from data sample to process

#### do not edit below this line ####

with open (tweets_store, 'r') as tweets_file:
	head = [next(tweets_file) for x in xrange(num_tweets_to_process)]
word_occurrences = {}
analysis_file = open (analysis_output_file, 'w')
occurrences_file = open (occurrences_output_file, 'w')

for word in tracked_words:
	word_occurrences[word] = { 'count' : 0, 'positive' : 0, 'negative' : 0, 'unknown' : 0 }

def word_in_text (needle, haystack):
	needle = needle.lower()
	haystack = haystack.lower()
	match = re.search(needle, haystack)
	if match:
		return True
	else:
		return False

analysis_request_data = []
tweets_with_id = {}

i = 0
for line in head:
	try: 
		i += 1
		tweet = json.loads(line)
		tweet_payload = { 'text' : tweet['text'], 'language_iso' : 'nld', 'id' : i }
		analysis_request_data.append(tweet_payload)
		tweets_with_id[i] = tweet
		tweets_with_id[i]['sentiment_class'] = 'unknown'
		tweets_with_id[i]['confidence_sentiment'] = 0
	except:
		print 'Tweet could not be decoded, skipping and continuing'
		continue

api_request = { 'data': { 'api_key' : ai_applied_apikey, 'call' : { 'return_original' : False, 'classifier' : 'default', 'data' : analysis_request_data } } }
payload = { 'request' : json.dumps(api_request) }
headers = { 'Content-type' : 'application/json' }
r = requests.post(ai_applied_endpoint, data=payload, headers=headers)
results = json.loads(r.text)

for result in results['response']['data']: # combine analysis results with tweets dict
	tweets_with_id[result['id']]['sentiment_class'] = result['sentiment_class']
	tweets_with_id[result['id']]['confidence_sentiment'] = result['confidence_sentiment']

for twid in tweets_with_id:
	for word in tracked_words:
		if word_in_text(word, tweets_with_id[twid]['text']):
			print 'Word %s is in tweet' % word
			word_occurrences[word]['count'] += 1
			if tweets_with_id[twid]['sentiment_class'] == 'positive':
				word_occurrences[word]['positive'] += 1
			elif tweets_with_id[twid]['sentiment_class'] == 'negative':
				word_occurrences[word]['negative'] += 1
			elif tweets_with_id[twid]['sentiment_class'] == 'unknown':
				word_occurrences[word]['unknown'] += 1

pprint(word_occurrences)

analysis_file.write(json.dumps(tweets_with_id, indent=4, sort_keys=True)) # write tweets + corresponding analysis to file
occurrences_file.write(json.dumps(word_occurrences, indent=4, sort_keys=True)) # write occurrences to file

print 'API Credits remaining: %s' % results['response']['quota']['remaining_credits']