import re
import json
import time
from datetime import datetime, timedelta
from pprint import pprint

def word_in_text (needle, haystack):
	needle = needle.lower()
	haystack = haystack.lower()
	match = re.search(needle, haystack)
	if match:
		return True
	else:
		return False

def parse_timeline (parties_mapping, timeframes, tweets_store, timeline_output=False):

	head = open (tweets_store, 'r')

	parties_file = open (parties_mapping, 'r')
	parties_str = parties_file.read()
	parties = json.loads(parties_str)

	timeframes_json = open (timeframes, 'r')
	timeframes_dict = json.load(timeframes_json)

	# create list of tweets from json
	tweets = []
	passed = 0
	for line in head:
		try:
			tweets.append(json.loads(line))
		except:
			passed  += 1
			pass
	print '>>> %d tweets could not be loaded and were passed' % passed


	matches = 0
	total = 0

	tweets_unfitted = []
	for tweet in tweets:
		tweet_has_match = False
		tweet_fits_timeframe = False
		# total += 1

		twtime = datetime.strptime(tweet['created_at'],'%a %b %d %H:%M:%S +0000 %Y')
		twtime +=timedelta(hours=1)
		hm = twtime.strftime('%H%M')
		hm = int(hm)

		for frame in timeframes_dict:
			
			tfstart = timeframes_dict[frame]['start']
			tfend   = timeframes_dict[frame]['end'] 

			if hm >= int(tfstart) and hm <= int(tfend):
				tweet_fits_timeframe = True
				for party in parties:
					party_has_match = False
					if party not in timeframes_dict[frame]['occurrences']:
						timeframes_dict[frame]['occurrences'][party] = 0
					for synonym in parties[party]:
						if word_in_text(synonym, tweet['text']) and party_has_match == False:
							tweet_has_match = True
							party_has_match = True
							matches +=1
							timeframes_dict[frame]['occurrences'][party] += 1

		if tweet_fits_timeframe == False:
			tweets_unfitted.append(hm)
			print 'A tweet could not be fitted in the timeframes'
		if tweet_has_match == False:
			print 'A tweet could not be matched'

	pprint (timeframes_dict)
	if timeline_output != False:
		output_file = open (timeline_output, 'w')
		json.dump(timeframes_dict, output_file, indent=4, sort_keys=True)
	# pprint (tweets_unfitted)
	print '%d tweets were matched' % matches

parse_timeline ('parties_mapped.json', 'timeframes.json', 'slotdebat_matched.json', 'timeframes_with_data.json')