import re
import sys

if re.search('.json', sys.argv[1]):
	tweet_file = open (sys.argv[1], 'r')
	if sys.argv[1] != sys.argv[2]:
		tweet_output = open (sys.argv[2], 'w')
		for line in tweet_file:
			if re.match(r'^\s*$', line):
				pass
			else:
				tweet_output.write(line)
	else:
		print 'Error: input file may not be same as output file.'
else:
	print 'Error: Input file does not have .json extension.'