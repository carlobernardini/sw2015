/**
 * VU Social Web 2015
 * Provincial Elections 2015 Poll Prediction and Sentiment Analysis
 **
 * @author Carlo Bernardini <1934481@student.vu.nl>
 * @author Husnu Tas <2525616@student.vu.nl>
 * @author Deepak Sukhu <1997424@student.vu.nl>
 **/

$(function(){ // document ready
    'use strict';

    var elements = {
        loading: $('#loading'),
        numtweets: $('#num-tweets'),
        numtracks: $('#num-tracks'),
        wordcloud: $('#word-cloud')
    },  player;

    elements.loading.toggleClass('hidden', false);

    player = new YT.Player('player', {
        height: '390',
        videoId: 'sKoz_rqf_lA',
        events: {
            'onReady': onPlayerReady
        }
    });

    function onPlayerReady(event) {
        $(event.target).addClass('embed-responsive-item');
    }

    // Load occurrences file
    $.getJSON('sentiment/sentiment_occurrences.json', function(occurrences){

        setTimeout(function(){ // hide loading animation after XHR success
            elements.loading.fadeOut();
        }, 1000);

        // initvars
        var chart, row, pid, 
            count_total = 0, 
            max_occ = 0,
            nulcol  = ['Mentions'],
            valcol  = ['Mentions'],
            keycol  = ['x'],
            outcome = 
            {
                /**
                 * Actual election outcomes
                 * Values indicate no. of seats
                 **/
                'VVD': 13,
                'CDA': 12,
                'D66': 10,
                'PVV': 9,
                'SP': 9,
                'PvdA': 8,
                'GroenLinks': 4,
                'ChristenUnie': 3,
                'PvdD': 2,
                '50Plus': 2,
                'SGP': 2
            },
            outcol = ['Seats'],
            i = 0;

        $.each(occurrences, function (party, occ){
            count_total += occ.count;
            pid = party.replace(/[^a-zA-Z0-9\w]/g,''); // normalize party name for use as ID
            if (i % 6 == 0 || i == 0) {
                row = $('<div />')
                    .addClass('row')
                    .appendTo('#donut-container');
            }
            i++;
            var wrapper = $('<div />')
                .addClass('col-md-2 col-sm-4 col-xs-6 clearfix')
                .attr('id', 'donut_' + pid)
                .appendTo(row);

            max_occ = (occ.count > max_occ) ? occ.count : max_occ;

            var pct_positive = ((occ.positive / occ.count) * 100) || 0;
                pct_positive = Math.round(pct_positive * 100) / 100; // round two decimals
            var pct_negative = ((occ.negative / occ.count) * 100) || 0;
                pct_negative = Math.round(pct_negative * 100) / 100;

            chart = c3.generate({
                bindto: '#donut_' + pid,
                data: {
                    columns: [
                        ['Positive', pct_positive],
                        ['Negative', pct_negative]
                    ],
                    type: 'donut'
                },
                donut: {
                    title: party,
                    width: 40
                },
                size: {
                    height: 200
                }
            });

            // populate bar chart data
            keycol.push(pid);
            nulcol.push(pid, max_occ );
            valcol.push(occ.count);
            outcol.push(outcome[pid]);
        });

        var barchart = c3.generate({
            bindto: '#bar-chart',
            data: {
                x: 'x',
                columns: [
                    keycol,
                    nulcol,
                    outcol
                ],
                axes: {
                    Mentions: 'y',
                    Seats: 'y2'
                },
                type: 'bar'
            },
            tooltip: {
                grouped: false
            },
            transition: {
                duration: 800
            },
            size: {
                height:300
            },
            bar: {
                width: {
                    ratio: 0.8
                }
            },
            axis: {
                y: {
                    label: 'Times mentioned in tweets'
                },
                y2: {
                    show: true,
                    label: 'Actual result (# seats)'
                },
                x: {
                    label: 'Political party',
                    type: 'category',
                    categories: ['Party']
                }
            }
        });
        barchart.load({
            columns: [
                keycol,
                valcol
            ]
        });
        elements.numtweets.text(String(count_total).replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1,"));
        elements.numtracks.text(i);
    });
    
    /**
     * Values for this chart were put out in another structure by our Python script,
     * therefore we chose to copy them manually to overcome the need of rewriting
     * the script and run it again to structure the data appropriately.
     **/
    var timeline_data = [
        ['x', '20:30-20:35', '20:35-20:40', '20:40-20:45', '20:45-20:50', '20:50-20:55', '20:55-21:00', '21:00-21:05', '21:05-21:10', '21:10-21:15', '21:15-21:20', '21:20-21:25', '21:25-21:30', '21:30-21:35', '21:35-21:40', '21:40-21:45', '21:45-21:50', '21:50-21:55'],
        ['PvdA',        0, 1, 4, 2, 3, 3,   31, 57, 18, 8,  4,  10, 18, 23, 9,  4,  2],
        ['D66',         0, 4, 4, 1, 2, 2,   3,  22, 33, 40, 32, 31, 18, 18, 72, 46, 20],
        ['CDA',         0, 0, 4, 1, 5, 3,   5,  10, 23, 24, 16, 24, 52, 51, 15, 15, 11],
        ['VVD',         0, 1, 1, 0, 2, 0,   1,  4,  28, 42, 39, 25, 12, 17, 8,  11, 23],
        ['PVV',         1, 0, 5, 8, 2, 5,   2,  4,  3,  7,  31, 29, 10, 8,  7,  18, 17],
        ['Groen Links', 3, 1, 5, 5, 3, 4,   1,  2,  0,  0,  0,  2,  3,  0,  0,  1,  0],
        ['ChristenUnie',0, 0, 0, 2, 2, 0,   1,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0],
        ['50Plus',      0, 1, 0, 0, 0, 3,   2,  6,  1,  0,  0,  0,  2,  2,  0,  1,  4],
        ['SP',          0, 1, 3, 9, 1, 5,   8,  19, 12, 8,  2,  2,  0,  11, 25, 23, 8],
        ['SGP',         0, 0, 0, 2, 4, 5,   0,  0,  0,  0,  0,  0,  0,  0,  0,  0,  0],
        ['PvdD',        0, 0, 0, 1, 0, 0,   1,  1,  0,  2,  1,  0,  5,  2,  0,  0,  0],
    ]

    /**
     * Calculate relative value per party for pie chart
     **/
    var timeline_relative = [], x_skipped = false;
    for (var col in timeline_data) {
        if (x_skipped) {
            var party, tweets_total = 0;
            for (var i in timeline_data[col]) {
                if (i == 0) party = timeline_data[col][i]
                else        tweets_total += timeline_data[col][i]
            }
            var tweets_relative = Math.round(tweets_total / i * 10) / 10;
            timeline_relative.push([party, tweets_relative])
        }
        x_skipped = true;
    }

    var video_position,
        playback_timeout = false,
        broadcast_start = new Date('2015','03','17','20','30');

    var timeline = c3.generate({
        bindto: '#timeseries',
        data: {
            type: 'spline',
            x: 'x',
            columns: timeline_data,
            onclick: function (d, element) {
                /**
                 * YouTube: Seek to selected timeframe
                 **/
                var categories = this.categories(),
                    category_i = d.index,
                    category_v = categories[category_i];

                var timeframe_start = category_v.split('-')[0],
                    timeframe_hrmin = timeframe_start.split(':'),
                    timeframe_start = new Date('2015','03','17',timeframe_hrmin[0],timeframe_hrmin[1]),
                    video_position = (timeframe_start - broadcast_start) / 1000;

                if (playback_timeout) clearTimeout(playback_timeout);
                
                playback_timeout = setTimeout(function(){
                    player.seekTo(video_position, true);
                    playback_timeout = false;
                }, 300);
            }
        },
        zoom: {
            enabled: true
        },
        legend: {
            position: 'right'
        },
        grid: {
            y: {
                show: true
            }
        },
        axis: {
            y: {
                label: {
                    text: 'Tweets',
                    position: 'inner-middle'
                }
            },
            x: {
                label: {
                    text: 'Time frames',
                    position: 'inner-center',
                    height: 100
                },
                type: 'category'
            }
        },
        size: {
            height: 375
        }
    });

    var piechart = c3.generate({
        bindto: '#pie-chart',
        data: {
            type: 'pie',
            columns: timeline_relative
        },
        legend: {
            position: 'bottom'
        },
        size: {
            height: 400
        }
    });

    var fill = d3.scale.category20();

    elements.loading.toggleClass('hidden', false);

    $.getJSON('textlabel/summary_extracted.json', function(data){

        elements.loading.toggleClass('hidden', true);

        var w = elements.wordcloud.width(),
            scale = 0.9,
            aspect = 600 / 800,
            h = Math.floor(w * aspect),
            words = [],
            fontSize = d3.scale['log']().range([10, 100]);

        var svg = d3.select("#word-cloud").append("svg")
            .attr("preserveAspectRatio", "xMidYMid meet")
            .attr("viewBox", "0 0 960 500")
            .attr("width", w)
            .attr("height", Math.floor(w * aspect));

        var background = svg.append("g"),
            vis = svg.append("g")
                    .attr("transform", "translate(" + [1000 >> 1, 500 >> 1] + ")scale(" + scale + ")");

        var layout = d3.layout.cloud();

        var populate_layout = function(layout, data) {
            layout
                .timeInterval(10)
                .size([w, h])
                .words(data.map(function(d){
                    return { text: d[0], size: Math.pow(3, Math.log(d[1])) / 100 }; // Logarithmic transformation of text size
                }))
                .fontSize(function(d) { return fontSize(+d.size); })
                .on("end", draw)
                .start();
            };
        populate_layout(layout, data);

        /**
         * Scale cloud SVG properly on window resize
         **/
        $(window).resize(function() {
            var width = elements.wordcloud.width();
            svg.attr("width", width);
            svg.attr("height", width * aspect); // Maintain aspect ratio
        });

        function draw(words, bounds) {
            scale = bounds ? Math.min(
              w / Math.abs(bounds[1].x - w / 2),
              w / Math.abs(bounds[0].x - w / 2),
              h / Math.abs(bounds[1].y - h / 2),
              h / Math.abs(bounds[0].y - h / 2)) / 2 : 1;
            var text = vis.selectAll("text")
              .data(words, function(d) { return d.text.toLowerCase(); });
            text.transition()
                .duration(1000)
                .attr("transform", function(d) { return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")"; })
                .style("font-size", function(d) { return d.size + "px"; });
            text.enter().append("text")
                .attr("text-anchor", "middle")
                .attr("transform", function(d) { return "translate(" + [d.x, d.y] + ")rotate(" + d.rotate + ")"; })
                .style("font-size", "1px")
                .transition()
                .duration(1000)
                .style("font-size", function(d) { return d.size + "px"; });
            text.style("font-family", function(d) { return d.font; })
                .style("fill", function(d) { return fill(d.text.toLowerCase()); })
                .text(function(d) { return d.text; });
            var exitGroup = background.append("g")
                .attr("transform", vis.attr("transform"));
            var exitGroupNode = exitGroup.node();
            text.exit().each(function() {
                exitGroupNode.appendChild(this);
            });
            exitGroup.transition()
                .duration(1000)
                .style("opacity", 1e-6)
                .remove();
        }

        $('.terms-action').on('click', function(){
            var el = $(this),
                d  = [];
            if (el.data('action')) {
                switch (el.data('action')) {
                    default:
                        if (el.hasClass('reset-inline') && !el.hasClass('hidden')) el.toggleClass('hidden', true);
                        d = data.slice(0,100);
                    break;
                    case 'filter':
                        var ignore_fuzzy = ['pvda','vvd','cda','d66','pvv','pvdd','partijvddieren','50plus','groenlinks','sgp','christenunie','bussemaker','rutte','schippers','buma','sophieintveld','demmink'];
                        var ignore_exact = ['sp'];
                        for (var i in data) {
                            if ( !(new RegExp( '\\' + ignore_fuzzy.join('|') + '\\gi') ).test(data[i][0]) &&
                                 !(new RegExp( '\\b' + ignore_exact.join('|') + '\\b') ).test(data[i][0]) ) {
                                d.push(data[i]);
                            }
                        }
                        $('button.reset-inline').toggleClass('hidden', false);
                    break;
                }
                if (!el.hasClass('reset-inline')) $('#filter-terms-dropdown').dropdown('toggle');
                populate_layout(layout, d);
            }
            return false;
        });
        $('')

        /**
         * Populate modal list of words
         **/
        var ul = $('#textlabel-list').find('ul.list-group'),
            li = $('<li />')
                .addClass('list-group-item');

        for (i in data) {
            var item = li.clone()
                .text(data[i][0])
                .append(
                    $('<span />')
                        .addClass('badge')
                        .text(Math.floor(data[i][1]))
                );
            item.appendTo(ul);
        }
    });

    $(window).resize(function(){
        var h = $(this).height() - 200;
        $('#textlabel-list')
            .find('div.modal-body')
            .css('max-height', h + 'px');
    }).trigger('resize');
});